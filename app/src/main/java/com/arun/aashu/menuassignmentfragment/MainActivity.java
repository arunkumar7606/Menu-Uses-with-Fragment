package com.arun.aashu.menuassignmentfragment;



import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);






    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.option_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        switch (id){

            case R.id.homeItem:
                FragmentManager fm=getSupportFragmentManager();
                FragmentTransaction ft= fm.beginTransaction();
                ft.replace(R.id.main_page,new FirstFragment());
                ft.commit();
                break;

            case R.id.aboutitem:
                FragmentManager fm1=getSupportFragmentManager();
                FragmentTransaction ft1= fm1.beginTransaction();
                ft1.replace(R.id.main_page,new AboutFragment());
                ft1.commit();
                break;

            case R.id.contactItem:
                FragmentManager fm3=getSupportFragmentManager();
                FragmentTransaction ft3= fm3.beginTransaction();
                ft3.replace(R.id.main_page,new ContactFragment());
                ft3.commit();
                break;

        }



        return super.onOptionsItemSelected(item);
    }
}
